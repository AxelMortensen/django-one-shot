from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemFrom

# Create your views here.
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list" : todo_list,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_item" : todo_list,
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save(False)
            todo_list.author = request.user
            todo_list.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()
    context = {
        "form" : form,
    }

    return render(request, "todos/create.html", context)

def todo_list_edit(request, id):
    edit_id = get_object_or_404(TodoList,id=id)

    if request.method == "POST":
        form = TodoListForm(request.POST, instance=edit_id)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=edit_id)
    context = {
        "form" : form
    }
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect(todo_list_list)
    else:
        form = TodoListForm()
    context = {
        "form" : form,
    }

    return render(request, "todos/delete.html", context)

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemFrom(request.POST)
        if form.is_valid():
            todo_list = form.save(False)
            todo_list.author = request.user
            todo_list.save()
            return redirect("todo_list_detail", id=todo_list.list.id)
    else:
        form = TodoItemFrom()
    context = {
        "form" : form,
    }

    return render(request, "todos/items/create.html", context)

def todo_item_update(request, id):
    edit_id = get_object_or_404(TodoItem,id=id)

    if request.method == "POST":
        form = TodoItemFrom(request.POST, instance=edit_id)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=edit_id.list.id)
    else:
        form = TodoItemFrom(instance=edit_id)
    context = {
        "form" : form
    }
    return render(request, "todos/items/edit.html", context)
